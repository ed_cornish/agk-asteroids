#pragma once
#include "ObjMgr.h"
#include "Constants.h"
#include "Bullet.h"
#include "Explosions.h"

/* Forward declaration of Bullet class */
class Bullet;

/* Used to cap the square of the player velocity */
#define VEL_CAP 350
#define HIT_DAMAGE 1

/* This class manages the player object */
class Player :
    public ObjMgr
{
public:
    /* We keep a local pointer to a Bullet class to manage the bullets fired by the player */
    Bullet *bullet_ptr;

    Explosions *explosions_ptr;

    UINT engine_particles_id;

    /* Constructor takes the image id for the player ship and a pointer to a Bullet object */
    Player(unsigned int img_id, Bullet *bullet_ptr_param, Explosions *explosions_ptr_param);

    /* Empty Destructor */
    ~Player();

    float player_health;

    /* Start life will be used as player health */
    float SetStartLife();

    /* Update the sprite based on Joystick input */
    /* If no joystick device is detected on the system, WSAD will be used */
    bool Update(UINT sprite);
};