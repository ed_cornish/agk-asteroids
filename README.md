# README #

### What is this repository for? ###

* This is a clone of the classic arcade game 'Asteroids', implemented in C++ using the AGK library (Tier 2)

### NOTES ###

* All the art and sound in this game are taken from OpenGameArt.org and are royalty-free as declared by their authors
* Specific attribution to be added

### TODO ###

* High scores not working
* Scoring needs adjustment
* There is no system of levels, once all the asteroids are gone, there is nothing to do!
* The original game had UFOs that would appear and attack the player...