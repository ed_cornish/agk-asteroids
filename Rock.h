#pragma once
#include "agk.h"
#include "ObjMgr.h"
#include "Explosions.h"

#define EXPLODE_IMPULSE 75
#define SCALE_STEP 0.6
#define MIN_SCALE  0.3
#define ROCK_ANGLE_MAX 180
#define ROCK_START_MASS 10000
#define ROCKS_PER_SPLIT 2
#define BASE_POINTS 5
#define NEW_INTERVAL 6

/* This class manages the Asteroids in the game (hereafter referred to as rocks for clarity) */
class Rock : public ObjMgr
{
public:
    Rock(unsigned int img_id, UINT num, Explosions *explosions_ptr_param);

    UINT GetPoints();
    /* Empty Destructor */
    ~Rock();

    /* Tick the internal timer and add another rock if it expires */
    void Increment();

protected:
    bool Update(UINT sprite);

    float timer;

    float SetStartLife();

    void SetRockPhysics(UINT sprite);

    /* This function creates two smaller Rocks in place of an existing rock */
    void SplitRock(UINT sprite_id);

    float points_accumulator;

    Explosions *explosions_ptr;
};

