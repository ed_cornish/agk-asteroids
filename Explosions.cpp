#include "Explosions.h"



Explosions::Explosions(UINT image_id) : ObjMgr(image_id)
{
   // Add(DEVICE_WIDTH / 2, DEVICE_HEIGHT / 2);
}


Explosions::~Explosions()
{
}

void Explosions::Add(float x, float y)
{
    object_list.emplace_back(Image_id, x, y);
    latest_add = object_list.back().id;
    agk::SetParticlesDepth(latest_add, PARTICLES_DEPTH);
}

bool Explosions::Update(UINT sprite)
{
    return false;
}

float Explosions::SetStartLife()
{
    return 0.0;
}