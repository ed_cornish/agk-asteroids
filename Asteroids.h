#ifndef _H_ASTEROIDS_
#define _H_ASTEROIDS_

// Link to GDK libraries
#include "AGK.h"
#include "ObjMgr.h"
#include <vector>
#include "Constants.h"

#define DEVICE_POS_X 32
#define DEVICE_POS_Y 32
#define FULLSCREEN false

typedef enum
{
    PLAYER,
    ROCK,
    EXPLOSION,
    BULLET,
    OBJ_CODE_MAX,
} obj_code_enum_t;


// Global values for the app
class app
{
	public:
        //Player's score
        UINT score;



		// constructor
		app() { memset ( this, 0, sizeof(app)); }

		// main app functions..
		void Begin( void );
		void Loop( void );
		void End( void );

        app_state_enum_t prev_app_state;
        app_state_enum_t app_state;
        void RunState(void);
        void CheckButtons(void);
        void RunGame(void);
};

extern app App;

#endif

// Allow us to use the LoadImage function name
#ifdef LoadImage
 #undef LoadImage
#endif