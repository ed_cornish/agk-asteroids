#pragma once
#include "ObjMgr.h"

struct particle_data
{
    UINT id;
    
    particle_data(UINT image_id, float x, float y) : id(agk::CreateParticles(x, y))
    {
        agk::ResetParticleCount(id);
        agk::SetParticlesFrequency(id, 500);
        agk::SetParticlesLife(id, 2);
        agk::SetParticlesSize(id, 64);
        agk::SetParticlesStartZone(id, -20, 0, 20, 0);
        agk::SetParticlesImage(id, image_id);
        agk::SetParticlesDirection(id, 10, 10);
        agk::SetParticlesAngle(id, 360);
        agk::SetParticlesVelocityRange(id, 1.6, 4.0);
        agk::SetParticlesMax(id, 60);


        agk::AddParticlesColorKeyFrame(id, 0.0, 0, 0, 0, 0);
        agk::AddParticlesColorKeyFrame(id, 0.25, 255, 255, 0, 180);
        agk::AddParticlesColorKeyFrame(id, 1.8, 255, 0, 0, 0);


        agk::AddParticlesForce(id, 2.0, 2.8, 40, -40);

    }
};

class Explosions :
    public ObjMgr
{
public:
    Explosions(UINT image_id);
    ~Explosions();

    list<particle_data> object_list;

    /* Use a sprite to determine the position and angle */
    void Add(float x, float y);

    bool Update(UINT sprite);

    float SetStartLife();

};

