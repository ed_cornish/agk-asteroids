#pragma once
#include "ObjMgr.h"
#include <ctime>
#include <deque>
#include <string>

/* Bullet speed per frame */
#define BULLET_SPEED 12
/* Bullet lifetime is seconds */
#define BULLET_LIFETIME_SECS 3.0
/* Time between firing bullets in seconds */
#define BULLET_CYCLE 0.5

class Bullet :
    public ObjMgr
{
public:
    /* Use this float to keep track of the last fired time, so that the fire rate of the bullets is limited */
    float last_fired;

    /* Constructor */
    Bullet(unsigned int img_id) : ObjMgr(img_id)
    {
        /* Bullets do not wrap */
        wrap_sprites = false;
        /* 0 init the last_fired float */
        last_fired = 0;

        agk::LoadSound(SND_LASER_ID, "Laser.wav");
    }

    /* Empty Destructor */
    ~Bullet()
    {
    }
    
    /* Fire a bullet, passing in the position and angle of the ship firing it */
    void Fire(float x_pos, float y_pos, float angle);

protected:
    bool Update(UINT sprite);

    /* This function checks the life value for the object (here used as a timeout
        tracker) and returns true if it has timed out. Otherwise the tracker is decremented. */
    bool CheckTime(UINT sprite);

    float SetStartLife();

    /* Move the bullet using the raycast method, return true if the bullet hit something */
    bool BulletMove(UINT sprite);
};
