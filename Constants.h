#pragma 

#ifndef _H_CONSTANTS_H_
#define _H_CONSTANTS_H_

//Define constants for screen size
#define DEVICE_WIDTH 1024
#define DEVICE_HEIGHT 768

#define STATIC_COLLISION_BODY 1
#define DYNAMIC_COLLISION_BODY 2
#define KINEMATIC_COLLISION_BODY 3

#define POLYGONAL_COLLISION_SHAPE 3

//Define colour values for RGBA inputs
#define COLOUR_VAL_FULL 255
#define COLOUR_VAL_75PC 200
#define COLOUR_VAL_50PC 130
#define COLOUR_VAL_25PC 65

#define ENGINE_PARTICLE_START_BOUNDS 2.5
#define ENGINE_PARTICLE_MIN_VEL 0.1
#define ENGINE_PARTICLE_MAX_VEL 0.5

#define ENGINE_PARTICLE_LIFE 0.5
#define ENGINE_PARTICLE_ANGLE 2
#define ENGINE_PARTICLE_KEY_FRAME_1 ENGINE_PARTICLE_LIFE * 0.2
#define ENGINE_PARTICLE_KEY_FRAME_2 ENGINE_PARTICLE_LIFE * 0.6
#define ENGINE_PARTICLE_KEY_FRAME_3 ENGINE_PARTICLE_LIFE * 0.8

#define ENGINE_PARTICLE_SCALE_POINT_1 0.1
#define ENGINE_PARTICLE_SCALE_POINT_2 0.8
#define ENGINE_PARTICLE_SCALE_POINT_3 1.6

#define ENGINE_PARTICLE_ON_FREQUENCY 500

#define PLAYER_DEPTH 1
#define PARTICLES_DEPTH 50

#define JOYSTICK_THRUST_SCALE 1000


#define GUI_NEW_GAME_STR    "New Game"
#define GUI_HOW_TO_STR      "How To Play"
#define GUI_HIGH_SCORES_STR "High Scores"
#define GUI_QUIT_STR        "Quit"
#define GUI_GAME_OVER_STR   "GAME OVER"

#define GUI_Y_NEW_GAME      (DEVICE_HEIGHT/2)-10
#define GUI_Y_HOW_TO_PLAY   (DEVICE_HEIGHT/2)-5
#define GUI_Y_HIGH_SCORES   (DEVICE_HEIGHT/2)+5
#define GUI_Y_QUIT          (DEVICE_HEIGHT/2)+10
#define GUI_Y_SCORE         10
#define GUI_Y_MID     DEVICE_HEIGHT/2

#define GUI_X_NEW_GAME      (DEVICE_WIDTH/2)
#define GUI_X_HOW_TO_PLAY   (DEVICE_WIDTH/2)
#define GUI_X_HIGH_SCORES   (DEVICE_WIDTH/2)
#define GUI_X_QUIT          (DEVICE_WIDTH/2)
#define GUI_X_SCORE         DEVICE_WIDTH - 30
#define GUI_X_MID     DEVICE_WIDTH/2

#define HOW_TO_TEXT_STR     "WSAD - Turn and Thrust\nSpacebar - Fire"

#define BTN_BASE_Y          DEVICE_HEIGHT/6
#define BTN_Y_INC           DEVICE_HEIGHT/10
#define BTN_DIAM            DEVICE_HEIGHT/15

#define BTN_FONT_SIZE       50

#define ESC_KEY_CODE    27

enum
{
    GUI_ID_HEALTH,
    GUI_ID_SCORE,
    GUI_ID_MENU_PAUSED,
    GUI_ID_MENU_NEW_GAME,
    GUI_ID_MENU_HIGH_SCORES,
    GUI_ID_MENU_HOW_TO,
    GUI_ID_MENU_QUIT,
    GUI_ID_GAME_OVER,
};

enum
{
    BTN_NEW_GAME = 1,
    BTN_HI_SCORES,
    BTN_HOW_TO,
    BTN_QUIT,
    BTN_YES,
    BTN_NO,
    BTN_BACK,
    BTN_MAX,
    HELP_TEXT,
    SCORE_TEXT,
};

typedef enum
{
    MENU_STATE,
    GAME_STATE,
    PAUSE_STATE,
    HOW_TO_STATE,
    HIGH_SCORES_STATE,
    GAME_OVER_STATE,
    QUIT_STATE,
    MAX_STATE,
    EXIT_STATE,
    PREV_STATE,
}app_state_enum_t;

enum
{
    PLAYER_IMG_ID = 1,
    ASTEROID_IMG_ID,
    BULLET_IMG_ID,
    EXPLOSION_IMG_ID,
    BG_IMG_ID,
    IMG_ID_MAX,
};

enum
{
    SND_LASER_ID = 1,
    SND_ROCK_BOOM_ID,
    SND_PLAYER_BOOM_ID,
    SND_PLAYER_HIT_ID,
    SND_GAME_OVER_ID,
    SND_MENU_OVER_ID,
    SND_THRUST_ID,
    SND_THRUST_TAIL_ID,
    SND_ID_MAX,
};

static const char *button_strings[BTN_MAX] = {
    "NEW GAME",
    "HIGH SCORES",
    "HOW TO PLAY",
    "QUIT",
    "YES",
    "NO",
    "BACK",
};

static const UINT button_x_vals[BTN_MAX] = {
    DEVICE_WIDTH / 2,
    DEVICE_WIDTH / 2,
    DEVICE_WIDTH / 2,
    DEVICE_WIDTH / 2,
    DEVICE_WIDTH / 3,
    DEVICE_WIDTH * 2 / 3,
    DEVICE_WIDTH / 3,
};

static const UINT button_y_vals[BTN_MAX] = {
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT / 8,
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT * 2 / 8,
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT * 3 / 8,
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT * 4 / 8,
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT * 4 / 8,
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT * 4 / 8,
    DEVICE_HEIGHT / 6 + DEVICE_HEIGHT * 5 / 8,
};

static const bool active_button_ids[MAX_STATE][BTN_MAX] = {
    //  NEW_GAME,   HI_SCORES,  HOW_TO  QUIT    YES     NO      BACK
        { true,     true,       true,   true,   false,  false,  false   }, //MENU_STATE
        { false,    false,      false,  false,  false,  false,  false   }, //GAME_STATE
        { false,    false,      true,   true,   false,  false,  true    }, //PAUSE_STATE
        { false,    false,      false,  false,  false,  false,  true    }, //HOW_TO_STATE
        { false,    false,      false,  false,  false,  false,  true    }, //HIGH_SCORES_STATE
        { false,    false,      true,   true,   false,  false,  true    }, //GAME_OVER_STATE
        { false,    false,      false,  false,  true,   true,   false   }, //QUIT_STATE
};

static const app_state_enum_t state_transition_table[MAX_STATE][BTN_MAX] = {
  //  NEW_GAME,     HI_SCORES,          HOW_TO          QUIT        YES         NO          BACK
    { GAME_STATE,   HIGH_SCORES_STATE,  HOW_TO_STATE,   QUIT_STATE, MAX_STATE,  MAX_STATE,  MAX_STATE }, //MENU_STATE
    { MAX_STATE,    MAX_STATE,          MAX_STATE,      MAX_STATE,  MAX_STATE,  MAX_STATE,  MAX_STATE }, //GAME_STATE
    { MAX_STATE,    MAX_STATE,          HOW_TO_STATE,   QUIT_STATE, MAX_STATE,  MAX_STATE,  GAME_STATE }, //PAUSE_STATE
    { MAX_STATE,    HIGH_SCORES_STATE,  HOW_TO_STATE,   QUIT_STATE, MAX_STATE,  MAX_STATE,  PREV_STATE }, //HOW_TO_STATE
    { MAX_STATE,    HIGH_SCORES_STATE,  HOW_TO_STATE,   QUIT_STATE, MAX_STATE,  MAX_STATE,  PREV_STATE }, //HIGH_SCORES_STATE
    { MAX_STATE,    MAX_STATE,          HOW_TO_STATE,   QUIT_STATE, MAX_STATE,  MAX_STATE,  MENU_STATE }, //GAME_OVER_STATE
    { MAX_STATE,    MAX_STATE,          MAX_STATE,      MAX_STATE,  EXIT_STATE, PREV_STATE, PREV_STATE }, //QUIT_STATE
};

static const char* how_to_string = "HOW TO PLAY:\nW - Forward Thrust\nS - Reverse Thrust\nA - Rotate Left\nD - Rotate Right\nSPACE - Fire\nESC - Pause Menu";
static const char* quit_prompt_string = "Are you sure you wish to quit?";
static const char* game_over_prompt_string = "GAME OVER...";

#endif