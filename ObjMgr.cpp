#include "ObjMgr.h"

void ObjMgr::Add(float x, float y)
{
    /* Add a new item to the object_list, this will internally construct the sprite from the image id */
    object_list.emplace_back(Image_id, SetStartLife());
    /* Set the latest_add member for ease of modification */
    latest_add = object_list.back().id;
    /* Set the sprite offset to the image centre (instead of the top left) */
    agk::SetSpriteOffset(latest_add,
        agk::GetSpriteWidth(latest_add) / 2,
        agk::GetSpriteHeight(latest_add) / 2);
    /* Set the sprite's position as specified */
    agk::SetSpritePositionByOffset(latest_add, x, y);
}

void ObjMgr::UpdateAll()
{
    /* Iterator to loop over all objects */
    list<object_data>::iterator list_it = object_list.begin();
    /* Set the end point */
    end = object_list.end();

    /* While we have not hit the end of the list... */
    while (list_it != end)
    {
        /* Store iterator for debugging */
        it = list_it;

        /* Run update function */
        if (Update(list_it->id))
        {
            /* Update returned true, we need to remove the object from the list */
            list_it = object_list.erase(list_it);
        }
        else
        {
            /* Wrap the sprite if the flag is set */
            if (wrap_sprites == true)
            {
                Wrap(list_it->id);
            }
            /* Get the next list item */
            ++list_it;
        }
    }

}

void ObjMgr::Wrap(unsigned int Sprite_id)
{
    float x_pos, y_pos = 0.0;
    float x_vel, y_vel = 0.0;
    bool clamp = FALSE;

    /* Get the position of the sprite */
    x_pos = agk::GetSpriteXByOffset(Sprite_id);
    y_pos = agk::GetSpriteYByOffset(Sprite_id);

    /* Get the current physics velocity so that we can retain it after we move the sprite */
    x_vel = agk::GetSpritePhysicsVelocityX(Sprite_id);
    y_vel = agk::GetSpritePhysicsVelocityY(Sprite_id);

    /* We have a margin around the edge to provide hysteresis */
    if (x_pos < -10)
    {
        x_pos = DEVICE_WIDTH;
        clamp = TRUE;
    }
    if (y_pos < -10)
    {
        y_pos = DEVICE_HEIGHT;
        clamp = TRUE;
    }

    if (x_pos >(DEVICE_WIDTH + 10))
    {
        x_pos = 0;
        clamp = TRUE;
    }
    if (y_pos > (DEVICE_HEIGHT + 10))
    {
        y_pos = 0;
        clamp = TRUE;
    }

    /* If we have to clamp the postion, set using AGK API */
    if (clamp)
    {
        agk::SetSpritePositionByOffset(Sprite_id, x_pos, y_pos);
        agk::SetSpritePhysicsVelocity(Sprite_id, x_vel, y_vel);
    }
}