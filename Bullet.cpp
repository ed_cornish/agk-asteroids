#include "Bullet.h"

/* Fire a bullet, passing in the position and angle of the ship firing it */
void Bullet::Fire(float x_pos, float y_pos, float angle) 
{
    /* Check the time since the last bullet was fired */
    float fire_interval = (agk::Timer() - last_fired);
    /* Fire interval must be greater than the BULLET_CYCLE interval */
    if (fire_interval > BULLET_CYCLE)
    {
        Add(x_pos, y_pos);
        ///* Add the bullet */
        //Add(x_pos, y_pos);
        ///* Set the angle to match that of the firing ship */
        agk::SetSpriteAngle(object_list.back().id, angle);
        /* Update the last_fired time */
        last_fired = agk::Timer();
        agk::PlaySound(SND_LASER_ID, 60);
    }

}

//Protected...
bool Bullet::Update(UINT sprite)
{
    /* If either the bullet has hit something or the bullet has timed out, remove it */
    bool kill = (BulletMove(sprite) || CheckTime(sprite));

   // Wrap(sprite);

    //agk::Print(last_fired);

    /* Return the kill indicator */
    return kill;
}

/* This function checks the life value for the object (here used as a timeout
tracker) and returns true if it has timed out. Otherwise the tracker is decremented. */
bool Bullet::CheckTime(UINT sprite)
{
    if (it->life < 0)
    {
        /* The bullet has timed out */
        return true;
    }
    else
    {
        /* Decrement the life value by the last frame interval */
        it->life -= agk::GetFrameTime();
    }
    /* Bullet has not timed out */
    return false;
}

float Bullet::SetStartLife()
{
    /* We use the life member as a timeout tracker, set it to the lifetime in secs */
    return BULLET_LIFETIME_SECS;
}

/* Move the bullet using the raycast method, return true if the bullet hit something */
bool Bullet::BulletMove(UINT sprite)
{
    UINT hit_sprite;
    /* Get the starting position of the bullet */
    float x_pos = agk::GetSpriteXByOffset(sprite);
    float y_pos = agk::GetSpriteYByOffset(sprite);

    /* Get the end point of the ray */
    float x_delta = sin(agk::GetSpriteAngleRad(sprite)) * BULLET_SPEED;
    float y_delta = -cos(agk::GetSpriteAngleRad(sprite)) * BULLET_SPEED;

    /* Use the flag to track if we hit something */
    bool hit = FALSE;

    /* Perform the raycast twice, to workaround a weird bug in AGK where raycasts sometimes go through sprites */
    if (agk::PhysicsRayCast(x_pos, y_pos, x_pos + x_delta, y_pos + y_delta)
        || agk::PhysicsRayCast(x_pos, y_pos, x_pos + x_delta, y_pos + y_delta))
    {
        //agk::Print("Hit!");
        /* Reduce the position delta by the raycast fraction, so that we can move the bullet to the
        hit position (do not want to overshoot) */
        x_delta = x_delta * agk::GetRayCastFraction();
        y_delta = y_delta * agk::GetRayCastFraction();
        /* Set the flag */
        hit = TRUE;
        /* Set the hit sprite invisible, this will be checked in the seperate update function */
        hit_sprite = agk::GetRayCastSpriteID();
        if (PLAYER_IMG_ID != agk::GetSpriteImageID(hit_sprite))
        {
            agk::SetSpriteVisible(agk::GetRayCastSpriteID(), 0);
        }
    }

    /* Move the bullet by the position delta */
    agk::SetSpritePositionByOffset(sprite, x_pos + x_delta, y_pos + y_delta);

    /* Return the indicator */
    return hit;
}