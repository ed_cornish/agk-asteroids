#ifndef _OBJMGR_H_
#define _OBJMGR_H_

#pragma once
#include <list>
#include <algorithm>
#include "agk.h"
#include "Asteroids.h"
#include "Constants.h"

using namespace std;

//This structure holds the basic information about each game object
struct object_data
{   
    //Sprite Id for the object, for referencing with AGK
    UINT id;
    //This is a multipurpose variable, for tracking things like lifetime, health, score value etc
    float life;    
    //Struct Constructor (Create Sprite in init list)
    object_data(UINT img_id, float life) : id(agk::CreateSprite(img_id)), life(life)
    {
        //agk::Print("Constructor called");
    }

    //Struct Destructor
    ~object_data()
    {
        //Clean up the sprite within AGK
        agk::DeleteSprite(id);
        //agk::Print("Destructor called");
    }
};

//This is the base class for object management classes
//Each specialised object manager inherits from this
class ObjMgr
{    
protected:
    /* AGK image id used for creating new sprites */
    UINT Image_id;
    /* List of object_data */
    list<object_data> object_list;

    /* Store iterators for debug */
    list<object_data>::iterator it, end;

    UINT id_range_start;
    UINT id_range_end;

    /* Use this id to track the last sprite added */
    UINT latest_add;

    /* Boolean flag to indicate that sprites should wrap at screen edges */
    bool wrap_sprites = true;

    /* The Update method will be overridden by each object type class */
    /* NOTE: Update returns true if the object should be removed from the game*/
    virtual bool Update(UINT sprite) = 0;

    /* The SetStartLife function will be overridden be each object type class*/
    virtual float SetStartLife(void) = 0;

    /* Wrap the sprite at the edge of the screen */
    void Wrap(unsigned int Sprite_id);

public:
    /*Basic constructor for any sprite object manager, takes an AGK image reference id */
    ObjMgr(unsigned int img_id)
    {
        //Store the image id for creating objects in AGK
        Image_id = img_id;
        //Zero init the latest_add member
        latest_add = 0;
    }

    /* Empty destructor */
    ~ObjMgr()
    {
    }

    /* Update every object managed */
    void UpdateAll();

    /* Member function to add an object to the object list */
    void Add(float x, float y);
};


#endif