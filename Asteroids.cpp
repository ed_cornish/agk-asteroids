// Includes
#include "Asteroids.h"
#include "Rock.h"
#include "Player.h"
#include "Bullet.h"
#include "Explosions.h"
#include "Constants.h"
#include <iostream>
#include <string>

app App;

/* Classes derived from ObjMgr */
Player *Player1_p = NULL;
Rock *Rocks_p;
Bullet *PlayerBullet_p;
Explosions *Explosions_p;

/* This function is called once at initialisation */
void app::Begin(void)
{
    UINT i;
    float bg_scale;
	agk::SetVirtualResolution (DEVICE_WIDTH, DEVICE_HEIGHT);
	agk::SetClearColor( 0,0,0 ); // black
	agk::SetSyncRate(60,0);
	agk::SetScissor(0,0,0,0);
    agk::SetPhysicsGravity(0, 0);
    //agk::SetPhysicsDebugOn();
    agk::SetPhysicsCCD(1);

    agk::LoadImage(PLAYER_IMG_ID, "green_ship.png");
    agk::LoadImage(BULLET_IMG_ID, "bullet.png");
    agk::LoadImage(EXPLOSION_IMG_ID, "explosion.png");
    agk::LoadImage(ASTEROID_IMG_ID, "a10000.png");
    agk::LoadImage(BG_IMG_ID, "space.png");
    agk::CreateSprite(BG_IMG_ID, BG_IMG_ID);

    agk::LoadSound(SND_MENU_OVER_ID, "menuover.wav");

    bg_scale = min(DEVICE_HEIGHT, DEVICE_WIDTH) / agk::GetImageHeight(BG_IMG_ID);

    agk::SetSpriteScale(BG_IMG_ID, bg_scale, bg_scale);
    agk::SetSpriteDepth(BG_IMG_ID, 1000);

/*
    mgr_table_ptr = new MgrTable();
*/
    /* No walls at screen edges to facilitate wrapping */
    agk::SetPhysicsWallBottom(0);
    agk::SetPhysicsWallTop(0);
    agk::SetPhysicsWallRight(0);
    agk::SetPhysicsWallLeft(0);

    for (i = BTN_NEW_GAME; i < BTN_MAX; i++)
    {
        agk::CreateText(i, button_strings[i - 1]);
        agk::SetTextAlignment(i, 1);
        agk::SetTextX(i, button_x_vals[i - 1]);
        agk::SetTextY(i, button_y_vals[i - 1]);
        agk::SetTextSize(i, 20);
    }
    agk::CreateText(HELP_TEXT, "");
    agk::SetTextAlignment(HELP_TEXT, 1);
    agk::SetTextX(HELP_TEXT, DEVICE_WIDTH/2 );
    agk::SetTextY(HELP_TEXT, DEVICE_HEIGHT/2);
    agk::SetTextSize(HELP_TEXT, 20);
    agk::SetTextVisible(HELP_TEXT, 0);
    app_state = MENU_STATE;
    prev_app_state = MENU_STATE;
}

/* This is the host function called every frame */
void app::Loop (void)
{
    RunState();
    agk::PrintC("FPS: ");
    agk::PrintC(agk::ScreenFPS());
    agk::PrintC("                ");
    agk::PrintC("Score: ");
    agk::Print(score);
    agk::Sync();
}


void app::End (void)
{

}

void app::RunState(void)
{
    app_state_enum_t temp_state;
    for (UINT i = BTN_NEW_GAME; i < BTN_MAX; i++)
    {
        agk::SetTextVisible(i, active_button_ids[app_state][i-1]);
        if (agk::GetTextHitTest(i, agk::ScreenToWorldX(agk::GetPointerX()), agk::ScreenToWorldY(agk::GetPointerY()))
            && agk::GetTextVisible(i))
        {
            if (agk::GetTextColorBlue(i) > 0)
            {
                agk::PlaySound(SND_MENU_OVER_ID, 50);
            }
            agk::SetTextColor(i, COLOUR_VAL_FULL, 0, 0);
            if (agk::GetPointerPressed())
            {
                temp_state = state_transition_table[app_state][i - 1];
                if (temp_state == PREV_STATE)
                {
                    app_state = prev_app_state;
                }
                else if (temp_state == EXIT_STATE)
                {
                    End();
                    exit(0);
                }
                else
                {
                    prev_app_state = app_state;
                    app_state = temp_state;
                }
                /* Setup new objects if we are starting a new game */
                if ((temp_state == GAME_STATE) && (prev_app_state == MENU_STATE))
                {
                    /* Create sprite managers here */
                    delete Explosions_p;
                    Explosions_p = new Explosions(EXPLOSION_IMG_ID);
                    delete PlayerBullet_p;
                    PlayerBullet_p = new Bullet(BULLET_IMG_ID);
                    delete Player1_p;
                    Player1_p = new Player(PLAYER_IMG_ID, PlayerBullet_p, Explosions_p);
                    delete Rocks_p;
                    Rocks_p = new Rock(ASTEROID_IMG_ID, 5, Explosions_p);

                }
            }
        }
        else
        {
            agk::SetTextColor(i, COLOUR_VAL_FULL, COLOUR_VAL_FULL, COLOUR_VAL_FULL);
        }
    }
    /* State specific handling here */
    switch (app_state)
    {
    case HOW_TO_STATE:
        agk::SetTextString(HELP_TEXT, how_to_string);
        agk::SetTextVisible(HELP_TEXT, 1);
        break;
    case HIGH_SCORES_STATE:
        break;
    case GAME_OVER_STATE:
        agk::SetTextString(HELP_TEXT, game_over_prompt_string);
        agk::SetTextVisible(HELP_TEXT, 1);
        break;
    case QUIT_STATE:
        agk::SetTextString(HELP_TEXT, quit_prompt_string);
        agk::SetTextVisible(HELP_TEXT, 1);
        break;
    case GAME_STATE:
        agk::SetTextVisible(HELP_TEXT, 0);
        RunGame();
        if (Player1_p->player_health <= 0)
        {
            app_state = GAME_OVER_STATE;
            prev_app_state = GAME_STATE;
        }
        break;
    case MENU_STATE:
        agk::SetTextVisible(HELP_TEXT, 0);
        break;
    case PAUSE_STATE:
        agk::SetTextVisible(HELP_TEXT, 0);
        break;
    default:
        break;

    }

    if (app_state != GAME_STATE)
    {
        /* Stop Physics updates for the frame */
        agk::StepPhysics(0);
        agk::SetSpriteColorAlpha(BG_IMG_ID, COLOUR_VAL_25PC);
    }
}

void app::CheckButtons(void)
{
    if (agk::GetRawKeyPressed(ESC_KEY_CODE))
    {
        prev_app_state = app_state;
        app_state = PAUSE_STATE;
    }

    /* Debug hook to force a game over */
    if (agk::GetRawKeyPressed(222))
    {
        Player1_p->player_health = 0;
    }
}

void app::RunGame(void)
{
    agk::SetSpriteColorAlpha(BG_IMG_ID, COLOUR_VAL_FULL);
    Player1_p->UpdateAll();
    Rocks_p->UpdateAll();
    //Rocks_p->Increment();
    PlayerBullet_p->UpdateAll();
    score += Rocks_p->GetPoints();
    CheckButtons();
}

