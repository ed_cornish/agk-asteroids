#include "Player.h"
#include "ObjMgr.h"

Player::Player(unsigned int img_id, Bullet *bullet_ptr_param, Explosions *explosions_ptr_param) : ObjMgr(img_id)
{
    /* Position the player ship in the middle of the screen */
    Add(DEVICE_WIDTH / 2.0, DEVICE_HEIGHT / 2.0);
    /* Use Dynamic body (2) for the player ship */
    agk::SetSpritePhysicsOn(latest_add, DYNAMIC_COLLISION_BODY);
    /* Set polygonal collision shape (3) for the player ship */
    agk::SetSpriteShape(latest_add, POLYGONAL_COLLISION_SHAPE);
    agk::SetSpriteDepth(latest_add, PLAYER_DEPTH);

    player_health = object_list.back().life;

    engine_particles_id = agk::CreateParticles(agk::GetSpriteXByOffset(latest_add), agk::GetSpriteYByOffset(latest_add));
    agk::SetParticlesFrequency(engine_particles_id, 0);
    agk::SetParticlesLife(engine_particles_id, 0.5);
    agk::SetParticlesSize(engine_particles_id, 32);
    agk::SetParticlesStartZone(engine_particles_id, -ENGINE_PARTICLE_START_BOUNDS, 0, ENGINE_PARTICLE_START_BOUNDS, 0);
    agk::SetParticlesImage(engine_particles_id, EXPLOSION_IMG_ID);
    agk::SetParticlesAngle(engine_particles_id, ENGINE_PARTICLE_ANGLE);
    agk::SetParticlesVelocityRange(engine_particles_id, ENGINE_PARTICLE_MIN_VEL, ENGINE_PARTICLE_MAX_VEL);

    agk::AddParticlesColorKeyFrame(
        engine_particles_id, 
        ENGINE_PARTICLE_KEY_FRAME_1, 
        COLOUR_VAL_FULL, 
        COLOUR_VAL_75PC, 
        COLOUR_VAL_75PC, 
        COLOUR_VAL_75PC);

    agk::AddParticlesColorKeyFrame(
        engine_particles_id, 
        ENGINE_PARTICLE_KEY_FRAME_2, 
        COLOUR_VAL_FULL,
        COLOUR_VAL_75PC,
        COLOUR_VAL_50PC, 
        COLOUR_VAL_50PC);

    agk::AddParticlesColorKeyFrame(
        engine_particles_id, 
        ENGINE_PARTICLE_KEY_FRAME_3, 
        0, 0, 0, 0);

    agk::AddParticlesScaleKeyFrame(
        engine_particles_id, 
        ENGINE_PARTICLE_KEY_FRAME_1, 
        ENGINE_PARTICLE_SCALE_POINT_1);
    agk::AddParticlesScaleKeyFrame(
        engine_particles_id, 
        ENGINE_PARTICLE_KEY_FRAME_2, 
        ENGINE_PARTICLE_SCALE_POINT_2);

    agk::AddParticlesScaleKeyFrame(
        engine_particles_id, 
        ENGINE_PARTICLE_KEY_FRAME_3, 
        ENGINE_PARTICLE_SCALE_POINT_3);

    agk::SetParticlesActive(engine_particles_id, 1);
    agk::SetParticlesDepth(engine_particles_id, PARTICLES_DEPTH);

    bullet_ptr = bullet_ptr_param;
    explosions_ptr = explosions_ptr_param;

    agk::LoadSound(SND_PLAYER_BOOM_ID, "explosion3.wav");
    agk::LoadSound(SND_PLAYER_HIT_ID, "playerhit.wav");
    agk::LoadSound(SND_THRUST_ID, "thruster.wav");
    agk::LoadSound(SND_THRUST_TAIL_ID, "thrusterend.wav");
}

Player::~Player()
{
}

float Player::SetStartLife()
{
    return 10.0;
}

bool Player::Update(UINT sprite)
{
    /* Grab current joystick input */
    float joy_x = agk::GetJoystickX();
    float joy_y = agk::GetJoystickY();
    /* Get the sprite to input */
    UINT id = object_list.back().id;
    /* Calculate the appropriately rotated thrust to apply */
    float x_thrust = -sin(agk::GetSpriteAngleRad(id)) * joy_y * JOYSTICK_THRUST_SCALE;
    float y_thrust = cos(agk::GetSpriteAngleRad(id)) * joy_y * JOYSTICK_THRUST_SCALE;
    /* Get current angle */
    float angle = agk::GetSpriteAngle(id);
    /* We will modify the velocity directly */
    float x_vel = agk::GetSpritePhysicsVelocityX(id);
    float y_vel = agk::GetSpritePhysicsVelocityY(id);
    /* Calculate the magnitude of the current velocity */
    float vel_mag = sqrtf(x_vel*x_vel + y_vel*y_vel);
    
    float x_pos = agk::GetSpriteXByOffset(id);
    float y_pos = agk::GetSpriteYByOffset(id);

    UINT contact_id = 0;
    bool contacts_left = false;

    //Cap the ship velocity using the magnitude
    if (vel_mag > VEL_CAP)
    {
        agk::SetSpritePhysicsVelocity(id, x_vel * VEL_CAP / vel_mag, y_vel * VEL_CAP / vel_mag);
    }
    //agk::Print(vel_mag);

    //Apply physics impulse to the ship 
    agk::SetSpritePhysicsImpulse(id,
        x_pos,
        y_pos,
        x_thrust,
        y_thrust);


    /* Joystick button 1 is the fire button, spacebar if no joystick detected */
    if (agk::GetButtonPressed(1))
    {
        /* Fire a bullet using the bullet object */
        bullet_ptr->Fire(x_pos, y_pos, angle);
    }
    contacts_left = agk::GetSpriteFirstContact(sprite);

    while(contacts_left)
    {
        agk::Print("Contacts counting...");

        object_list.back().life -= HIT_DAMAGE;
        player_health = object_list.back().life;
        agk::Print("Hit!");
        agk::PlaySound(SND_PLAYER_HIT_ID, 60);
        contacts_left = agk::GetSpriteNextContact();
    }

    agk::Print(object_list.back().life);
    if (object_list.back().life <= 0)
    {
        agk::Print("GAME OVER!");
        agk::PlaySound(SND_PLAYER_BOOM_ID);
        agk::SetParticlesFrequency(engine_particles_id, 0);
        explosions_ptr->Add(x_pos, y_pos);
        return true;
    }

    /* Rotate the player ship according to the joystick X axis */
    agk::SetSpriteAngle(id, angle + joy_x * 10);

    if (joy_y != 0)
    {
        agk::SetParticlesDirection(engine_particles_id, -x_thrust, -y_thrust);
        agk::SetParticlesFrequency(engine_particles_id, ENGINE_PARTICLE_ON_FREQUENCY);
        agk::SetParticlesPosition(engine_particles_id, x_pos, y_pos);
        if (agk::GetSoundsPlaying(SND_THRUST_ID) == 0)
        {
            agk::PlaySound(SND_THRUST_ID, 40, 1);
        }
    }
    else
    {
        agk::SetParticlesFrequency(engine_particles_id, 0);
        if (agk::GetSoundsPlaying(SND_THRUST_ID))
        {
            agk::StopSound(SND_THRUST_ID);
            agk::PlaySound(SND_THRUST_TAIL_ID, 40);
        }
    }

    return false;
}