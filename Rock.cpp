#include "Rock.h"


Rock::Rock(unsigned int img_id, UINT num, Explosions *explosions_ptr_param) : ObjMgr(img_id)
{
    unsigned int i = 0;
    float x, y;

    points_accumulator = 0;
    timer = 0;

    for (i = 0; i < num; i++)
    {
        if (i % 2 == 0)
        {
            x = agk::Random(DEVICE_WIDTH * 0.6, DEVICE_WIDTH);
            y = agk::Random(DEVICE_HEIGHT * 0.6, DEVICE_HEIGHT);
        }
        else
        {
            x = agk::Random(0, DEVICE_WIDTH * 0.3);
            y = agk::Random(0, DEVICE_HEIGHT * 0.3);
        }
        Add(x, y);

        SetRockPhysics(latest_add);
    }

    explosions_ptr = explosions_ptr_param;
    agk::LoadSound(SND_ROCK_BOOM_ID, "explosion2.wav");
}

UINT Rock::GetPoints()
{
    float points = points_accumulator;

    points_accumulator = 0;

    return ceil(points);
}

Rock::~Rock()
{
}

//Protected...
bool Rock::Update(UINT sprite)
{
    if (agk::GetSpriteVisible(sprite))
    {
        /* Use the sprite visibility flag to determine if the Rock
        has been destroyed. If it has (sprite is not visible), split the Rock and remove it */
        return false;
    }
    else
    {
        SplitRock(sprite);
        this->points_accumulator += BASE_POINTS/agk::GetSpriteScaleX(sprite);
        agk::PlaySound(SND_ROCK_BOOM_ID, 80);
        return true;
    }
}

float Rock::SetStartLife()
{
    return 5.0;
}

void Rock::SetRockPhysics(UINT sprite)
{
    agk::SetSpriteAngle(sprite, agk::Random(0, ROCK_ANGLE_MAX));
    /* Use dynamic body physics for rocks (2) */
    agk::SetSpritePhysicsOn(sprite, 2);
    /* Rocks need to be relatively massive */
    agk::SetSpritePhysicsMass(sprite, ROCK_START_MASS * agk::GetSpriteScaleX(sprite));
    /* Give the rock a random starting velocity */
    agk::SetSpritePhysicsVelocity(sprite, agk::RandomSign(agk::Random(0, 100)), agk::RandomSign(agk::Random(0, 100)));
    /* Give a slight spin to the rock */
    agk::SetSpritePhysicsAngularVelocity(sprite, agk::RandomSign(agk::Random(0, 2)));
    /* Set 0 damping */
    agk::SetSpritePhysicsDamping(sprite, 0);
    /* Set polygonal collision shape */
    agk::SetSpriteShape(sprite, 3);
    /* Setup a nominal amount of friction */
    agk::SetSpritePhysicsFriction(sprite, 0);
    /* Bouncy collisions */
    agk::SetSpritePhysicsRestitution(sprite, 1);
    agk::SetSpritePhysicsIsBullet(sprite, 1);
}

/* This function creates two smaller Rocks in place of an existing rock */
void Rock::SplitRock(UINT sprite_id)
{
    float imp_x, imp_y, scale;

    //Set up a random impulse for the split rocks
    imp_x = agk::RandomSign(agk::Random(1500, 2000)) * 10;
    imp_y = agk::RandomSign(agk::Random(1500, 2000)) * 10;

    scale = agk::GetSpriteScaleX(sprite_id) * SCALE_STEP;
    if (scale > MIN_SCALE)
    {
        for (UINT i = 0; i < ROCKS_PER_SPLIT; i++)
        {
            Add(agk::GetSpriteXByOffset(sprite_id),
                agk::GetSpriteYByOffset(sprite_id));
            agk::SetSpriteScaleByOffset(latest_add, scale, scale);
            SetRockPhysics(latest_add);
            agk::SetSpritePhysicsImpulse(latest_add,
                agk::GetSpriteXByOffset(sprite_id),
                agk::GetSpriteYByOffset(sprite_id),
                (agk::GetSpriteXByOffset(latest_add) - agk::GetSpriteXByOffset(sprite_id)) * EXPLODE_IMPULSE,
                (agk::GetSpriteYByOffset(latest_add) - agk::GetSpriteYByOffset(sprite_id)) * EXPLODE_IMPULSE);
        }
    }

    explosions_ptr->Add(agk::GetSpriteXByOffset(sprite_id), agk::GetSpriteYByOffset(sprite_id));
}

void Rock::Increment()
{
    timer += agk::GetFrameTime();

    if (timer > NEW_INTERVAL)
    {
        timer = 0;
        Add(agk::RandomSign(DEVICE_WIDTH+10), agk::RandomSign(DEVICE_HEIGHT+10));
        SetRockPhysics(latest_add);
    }

}